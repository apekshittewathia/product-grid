function AJAXHandler(filePath, successCallback, failureCallback) {
  this.filePath = filePath;
  this.successCallback = successCallback;
  this.failureCallback = failureCallback;
}

AJAXHandler.prototype.init = function() {
  var that = this;
  $.ajax({
    url: this.filePath,
    type: "GET",
    dataType: "json"
  }).done(function(response) {
    that.response = response;
    that.successCallback(response);
  }).fail(function(xhr, status, errorThrown) {
    that.successCallback();
    alert("Sorry, there was a problem!");
    console.log("Error: " + errorThrown);
    console.log("Status: " + status);
    console.log(xhr);
  });
};

//---------------------------------------------------------

function ProductsHandler() {
  this.products = [];
  this.$productsContainer = $("[data-type=products-container]");
  this.productsPerPage = 3;
  this.numberOfPages = -1;
  this.currentPage = 1;
  this.currentSortingBy = "";
}

ProductsHandler.prototype.init = function(products, setCurrentPageCallback, currentPage) {
  this.products = products;
  this.setCurrentPageCallback = setCurrentPageCallback;
  this.sortProducts();
  this.currentPage = currentPage;
  this.initUpdatePages();
  this.showProducts();
  this.bindEvents();
};

ProductsHandler.prototype.updateProductsPerPage = function(productsPerPage) {
  this.productsPerPage = productsPerPage;
  this.updatePages();
  this.showProducts();
};

ProductsHandler.prototype.updateProducts = function(products) {
  this.products = products;
  this.update();
};

ProductsHandler.prototype.sortProductsBy = function(type) {
  this.currentSortingBy = type;
  this.update();
};

ProductsHandler.prototype.update = function() {
  this.updatePages();
  this.sortProducts();
  this.showProducts();
};

ProductsHandler.prototype.initUpdatePages = function() {
  this.numberOfPages = this.products.length / this.productsPerPage;
  this.$productsContainer.empty();
  this.$productsContainer.append(this.getPaginationStructure());
}

ProductsHandler.prototype.sortProducts = function() {
  var type = this.currentSortingBy;
  this.products.sort(function(a, b) {
    if (a[type] > b[type]) {
      return 1;
    }
    if (a[type] < b[type]) {
      return -1;
    }
    return 0;
  });
};

ProductsHandler.prototype.updatePages = function() {
  this.numberOfPages = this.products.length / this.productsPerPage;
  this.setCurrentPage(1);
  this.$productsContainer.empty();
  this.$productsContainer.append(this.getPaginationStructure());
};

ProductsHandler.prototype.showProducts = function() {
  this.$productsContainer.children("div[data-type=products-div]").remove();
  this.$productsContainer.prepend(this.getProductsStructure());
};

ProductsHandler.prototype.getProductsStructure = function() {
  var $productsDiv = $("<div data-type='products-div'/>");
  var startIndex = (this.currentPage - 1) * this.productsPerPage;
  var endIndex = startIndex + this.productsPerPage;
  this.products.slice(startIndex, endIndex).forEach(function(item, index, array) {
    var $productDiv = $("<div class='product-div'/>");
    $productDiv.append("<img src='product_data/images/" + item.url + "'>");
    $productDiv.append("<div>" + item.name + "</div>");
    $productDiv.append("<div>" + item.color + "</div>");
    $productDiv.append("<div>" + item.brand + "</div>");
    $productsDiv.append($productDiv);
  });
  return $productsDiv;
};

ProductsHandler.prototype.getPaginationStructure = function() {
  var paginationDiv = $("<div class='pagination' data-type='pagination'/>");
  for (var i = 0; i < this.numberOfPages; i++) {
    paginationDiv.append("<a href='' value=" + (i + 1) + ">" + (i + 1) + "</a>");
  }
  paginationDiv.find("a").eq(this.currentPage - 1).css("background-color", "red");
  return paginationDiv;
};

ProductsHandler.prototype.setCurrentPage = function(value) {
  this.currentPage = value;
  this.setCurrentPageCallback(value);
}

ProductsHandler.prototype.bindEvents = function() {
  var that = this;
  this.$productsContainer.on("click", "div[data-type=pagination] a", function(event) {
    event.preventDefault();
    that.setCurrentPage(parseInt(this.getAttribute("value")));
    var $this = $(this);
    $this.css("background-color", "red");
    $this.siblings().css("background-color", "");
    that.showProducts();
  });
};

//---------------------------------------------------------

function FiltersHandler(products) {
  this.products = products;
  this.paginations = [3, 6, 12, 20];
  this.sortings = ["name", "brand", "color", "sold_out"];
  this.$filtersContainer = $("[data-type=filters-container]");
  this.filters = {
    color: {
      type: "String",
      name: "Colors"
    },
    brand: {
      type: "String",
      name: "Brands"
    },
    sold_out: {
      type: "Boolean",
      name: "Available"
    }
  };
  this.productsHandler = new ProductsHandler();
  this.$paginationSelect = null;
  this.defaultValues = null;
  this.$sortingSelect = null;
}

FiltersHandler.prototype.setDefaultValues = function(){
  try {
    this.defaultValues = JSON.parse(decodeURI(window.location.hash.substr(1)));
  } catch {
    console.log('Error');
    this.defaultValues = {
      filter: {
        color: [],
        brand: [],
        sold_out: []
      },
      pagination: 6,
      sorting: "color",
      currentPage: 1
    };
    window.location.hash = JSON.stringify(this.defaultValues);
  }
}

FiltersHandler.prototype.init = function() {
  this.setFilterObject(this.filters);
  this.setDefaultValues();
  this.$filtersContainer.append(this.getHTMLStructure());
  this.bindEvents();
  this.productsHandler.init(this.getFilteredProducts(), this.setCurrentPage.bind(this), parseInt(this.defaultValues.currentPage));
};

FiltersHandler.prototype.setCurrentPage = function(value){
  this.defaultValues.currentPage = value;
  window.location.hash = JSON.stringify(this.defaultValues);
}

FiltersHandler.prototype.setFilterObject = function(filters) {
  var filterObject = {};
  var filterTypes = Object.keys(filters);
  for (var i = 0; i < filterTypes.length; i++) {
    filterObject[filterTypes[i]] = {};
  }
  this.products.forEach(function(item, index, array) {
    var outerItem = item;
    for (var i = 0; i < filterTypes.length; i++) {
      filterObject[filterTypes[i]][outerItem[filterTypes[i]]] = 1;
    }
  });
  Object.keys(filters).forEach(function(item, index, array) {
    filters[item].values = Object.keys(filterObject[item]);
  });
  console.log(filters);
};

FiltersHandler.prototype.getHTMLStructure = function() {
  var $filtersDiv = this.getFiltersStructure();
  this.$paginationSelect = this.getPaginationSelectStructure();
  $filtersDiv.append(this.$paginationSelect);
  this.$sortingSelect = this.getSortingSelectStructure();
  $filtersDiv.append(this.$sortingSelect);
  return $filtersDiv;
};

FiltersHandler.prototype.getFiltersStructure = function() {
  var that = this;
  var filters = this.filters;
  var $filtersDiv = $("<div data-type='filters-div'/>");
  Object.keys(filters).forEach(function(item, index, array) {
    var $filterCheckboxes = $("<div data-type='filter-div' class='filter-div'/>");
    $filterCheckboxes.append("<div>" + filters[item]["name"] + "</div>");
    var filterType = item;
    var checked = "";
    if (filters[item]["type"] === "Boolean") {
      checked = "";
      if (that.defaultValues.filter[item].length > 0) {
        checked = "checked";
      }
      $filterCheckboxes.append("<input type='checkbox' value='" + item + "' name=" + item + " data-type=" + item + " " + checked + ">" + filters[item]["name"] + "<br>");
    } else {
      filters[item]["values"].forEach(function(item, index, array) {
        checked = "";
        if (that.defaultValues.filter[filterType].indexOf(item) != -1) {
          checked = "checked";
        }
        $filterCheckboxes.append("<input type='checkbox' value='" + item + "' name=" + filterType + " data-type=" + filterType + " " + checked + ">" + item + "<br>");
      });
    }
    $filterCheckboxes.append("<br>");
    $filtersDiv.append($filterCheckboxes);
  });
  return $filtersDiv;
}

FiltersHandler.prototype.getSortingSelectStructure = function() {
  var $sortingSelect = $("<select data-type='sorting'/>");
  this.sortings.forEach(function(item, index, array) {
    $sortingSelect.append("<option data-value=" + item + ">" + item + "</option>");
  });
  this.productsHandler.currentSortingBy = this.defaultValues.sorting;
  console.log(this.defaultValues.sorting);
  $sortingSelect.find("option[data-value=" + this.defaultValues.sorting + "]").attr("selected", "selected");
  return $sortingSelect;
}


FiltersHandler.prototype.getPaginationSelectStructure = function() {
  var $paginationSelect = $("<select data-type='pagination'/>");
  this.paginations.forEach(function(item, index, array) {
    $paginationSelect.append("<option data-value=" + item + ">" + item + "</option>");
  });
  this.productsHandler.productsPerPage = this.defaultValues.pagination;
  $paginationSelect.find("option[data-value=" + this.defaultValues.pagination + "]").attr("selected", "selected");
  return $paginationSelect;
}

FiltersHandler.prototype.getFilteredProducts = function() {
  var filters = this.filters;
  var filteredProducts = this.products.slice(0);
  var that = this;
  Object.keys(filters).forEach(function(item, index, array) {
    var filterType = item;
    var checkedElements = that.$filtersContainer.find("input[data-type=" + item + "]:checked");
    if (checkedElements.length === 0) {
      return;
    }
    if (filters[item]["type"] == "Boolean") {
      filteredProducts = filteredProducts.filter(function(item, index, array) {
        return item[filterType] == false;
      });
    } else {
      filteredProducts = filteredProducts.filter(function(item, index, array) {
        for (var i = 0; i < checkedElements.length; i++) {
          if (item[filterType] == checkedElements[i].value) {
            return true;
          }
        }
        return false;
      });
    }
  });
  return filteredProducts;
};

FiltersHandler.prototype.bindEvents = function() {
  var that = this;
  this.$filtersContainer.find("input[type=checkbox]").click(function() {
    that.updateHash($(this));
    that.productsHandler.updateProducts(that.getFilteredProducts());
  });

  this.$paginationSelect.on("change", function() {
    that.updatePaginationHash($(this));
    that.productsHandler.updateProductsPerPage(parseInt(this.value));
  });

  this.$sortingSelect.on("change", function() {
    that.updateSortingHash($(this));
    that.productsHandler.sortProductsBy(this.value);
  });
};

FiltersHandler.prototype.updatePaginationHash = function(element) {
  this.defaultValues[element.attr("data-type")] = parseInt(element.val());
  window.location.hash = JSON.stringify(this.defaultValues);
}

FiltersHandler.prototype.updateSortingHash = function(element) {
  this.defaultValues[element.attr("data-type")] = element.val();
  window.location.hash = JSON.stringify(this.defaultValues);
}

FiltersHandler.prototype.updateHash = function(element) {
  var filterArray = this.defaultValues.filter[element.attr("data-type")];
  var index = filterArray.indexOf(element.val());
  if (index !== -1) {
    filterArray.splice(index, 1);
  } else {
    filterArray.push(element.val());
  }
  window.location.hash = JSON.stringify(this.defaultValues);
}

//---------------------------------------------------------

function Controller() {}

Controller.prototype.init = function() {
  var ajaxHandler = new AJAXHandler("product.json", this.loadContent, function() {});
  ajaxHandler.init();
}

Controller.prototype.loadContent = function(response) {
  var filtersHandler = new FiltersHandler(response);
  filtersHandler.init();
};

$(document).ready(function() {
  var controller = new Controller();
  controller.init();
});