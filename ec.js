function AJAXHandler(filePath, successCallback, failureCallback) {
  this.filePath = filePath;
  this.successCallback = successCallback;
  this.failureCallback = failureCallback;
}

AJAXHandler.prototype.init = function() {
  var that = this;
  $.ajax({
    url: this.filePath,
    type: "GET",
    dataType: "json"
  }).done(function(response) {
    that.response = response;
    that.successCallback(response);
  }).fail(function(xhr, status, errorThrown) {
    that.successCallback();
    alert("Sorry, there was a problem!");
    console.log("Error: " + errorThrown);
    console.log("Status: " + status);
    console.log(xhr);
  });
};

//---------------------------------------------------------

function ProductsHandler() {
  this.products = [];
  this.$productsContainer = $("[data-type=products-container]");
  this.productsPerPage = 3;
  this.numberOfPages = -1;
  this.currentPage = 1;
  this.currentSortingBy = "";
}

ProductsHandler.prototype.init = function(products) {
  this.products = products;
  this.sortProducts();
  this.initUpdatePages();
  this.showProducts();
  this.bindEvents();
};

ProductsHandler.prototype.updateProductsPerPage = function(productsPerPage) {
  this.productsPerPage = productsPerPage;
  this.updatePages();
  this.showProducts();
};

ProductsHandler.prototype.updateProducts = function(products) {
  this.products = products;
  this.update();
};

ProductsHandler.prototype.sortProductsBy = function(type) {
  this.currentSortingBy = type;
  this.update();
};

ProductsHandler.prototype.update = function() {
  this.updatePages();
  this.sortProducts();
  this.showProducts();
};

ProductsHandler.prototype.initUpdatePages = function() {
  var index = window.location.hash.indexOf('currentpage=');
  if (index !== -1) {
    this.currentPage = parseInt(window.location.hash[index + 12]);
  } else {
    this.currentPage = 1;
  }
  this.numberOfPages = this.products.length / this.productsPerPage;
  this.$productsContainer.empty();
  this.$productsContainer.append(this.getPaginationStructure());
}

ProductsHandler.prototype.sortProducts = function() {
  var type = this.currentSortingBy;
  this.products.sort(function(a, b) {
    if (a[type] > b[type]) {
      return 1;
    }
    if (a[type] < b[type]) {
      return -1;
    }
    return 0;
  });
};

ProductsHandler.prototype.updatePages = function() {
  this.numberOfPages = this.products.length / this.productsPerPage;
  this.currentPage = 1;
  this.updatePageHash();

  this.$productsContainer.empty();
  this.$productsContainer.append(this.getPaginationStructure());
};

ProductsHandler.prototype.showProducts = function() {
  this.$productsContainer.children("div[data-type=products-div]").remove();
  this.$productsContainer.prepend(this.getProductsStructure());
};

ProductsHandler.prototype.getProductsStructure = function() {
  var $productsDiv = $("<div data-type='products-div'/>");
  var startIndex = (this.currentPage - 1) * this.productsPerPage;
  var endIndex = startIndex + this.productsPerPage;
  this.products.slice(startIndex, endIndex).forEach(function(item, index, array) {
    var $productDiv = $("<div class='product-div'/>");
    $productDiv.append("<img src='product_data/images/" + item.url + "'>");
    $productDiv.append("<div>" + item.name + "</div>");
    $productDiv.append("<div>" + item.color + "</div>");
    $productDiv.append("<div>" + item.brand + "</div>");
    $productsDiv.append($productDiv);
  });
  return $productsDiv;
};

ProductsHandler.prototype.getPaginationStructure = function() {
  var paginationDiv = $("<div class='pagination' data-type='pagination'/>");
  for (var i = 0; i < this.numberOfPages; i++) {
    paginationDiv.append("<a href='' value=" + (i + 1) + ">" + (i + 1) + "</a>");
  }
  paginationDiv.find("a").eq(this.currentPage - 1).css("background-color", "red");
  return paginationDiv;
};

ProductsHandler.prototype.bindEvents = function() {
  var that = this;
  this.$productsContainer.on("click", "div[data-type=pagination] a", function(event) {
    event.preventDefault();
    window.location.hash = window.location.hash.replace(/,currentpage=\d+/, "");
    window.location.hash += (",currentpage=" + parseInt(this.getAttribute("value")));
    var $this = $(this);
    $this.css("background-color", "red");
    $this.siblings().css("background-color", "");
    that.currentPage = parseInt(this.getAttribute("value"));
    that.showProducts();
  });
};

ProductsHandler.prototype.updatePageHash = function() {
  window.location.hash = window.location.hash.replace(/,currentpage=\d+/, "");
  window.location.hash += (",currentpage=" + this.currentPage);
}

//---------------------------------------------------------

function FiltersHandler(products) {
  this.products = products;
  this.$filtersContainer = $("[data-type=filters-container]");
  var filterObject = this.getFilterObject("color", "brand", "sold_out");
  this.filters = {
    color: {
      values: Object.keys(filterObject.color),
      type: "String",
      name: "Colors"
    },
    brand: {
      values: Object.keys(filterObject.brand),
      type: "String",
      name: "Brands"
    },
    sold_out: {
      values: Object.keys(filterObject.sold_out),
      type: "Boolean",
      name: "Available"
    }
  };
  this.productsHandler = new ProductsHandler([], -1);
  this.$paginationSelect = null;
}

FiltersHandler.prototype.getFilterObject = function() {
  var filterObject = {};
  for (var i = 0; i < arguments.length; i++) {
    filterObject[arguments[i]] = {};
  }
  var args = arguments;
  this.products.forEach(function(item, index, array) {
    var outerItem = item;
    for (var i = 0; i < args.length; i++)
      filterObject[args[i]][outerItem[args[i]]] = 1;
  });
  return filterObject;
};

FiltersHandler.prototype.init = function() {
  this.$filtersContainer.append(this.getHTMLStructure());
  this.bindEvents();
  this.productsHandler.init(this.getFilteredProducts());
};


FiltersHandler.prototype.getHTMLStructure = function() {
  var $filtersDiv = this.getFiltersStructure();
  this.$paginationSelect = this.getPaginationSelectStructure();
  $filtersDiv.append(this.$paginationSelect);
  this.$sortingSelect = this.getSortingSelectStructure();
  $filtersDiv.append(this.$sortingSelect);
  return $filtersDiv;
};

FiltersHandler.prototype.getFiltersStructure = function() {
  var filters = this.filters;
  var $filtersDiv = $("<div data-type='filters-div'/>");
  Object.keys(filters).forEach(function(item, index, array) {
    var $filterCheckboxes = $("<div data-type='filter-div' class='filter-div'/>");
    $filterCheckboxes.append("<div>" + filters[item]["name"] + "</div>");
    var filterType = item;
    var checked = "";
    if (filters[item]["type"] === "Boolean") {
      checked = "";
      if (window.location.hash.indexOf(item) != -1) {
        checked = "checked";
      }
      $filterCheckboxes.append("<input type='checkbox' value='" + item + "' name=" + item + " data-type=" + item + " " + checked + ">" + filters[item]["name"] + "<br>");
    } else {
      filters[item]["values"].forEach(function(item, index, array) {
        checked = "";
        if (window.location.hash.indexOf(item.replace(" ", "%20")) != -1) {
          checked = "checked";
        }
        $filterCheckboxes.append("<input type='checkbox' value='" + item + "' name=" + filterType + " data-type=" + filterType + " " + checked + ">" + item + "<br>");
      });
    }
    $filterCheckboxes.append("<br>");
    $filtersDiv.append($filterCheckboxes);
  });
  return $filtersDiv;
}

FiltersHandler.prototype.getSortingSelectStructure = function() {
  var $sortingSelect = $("<select data-type='sorting'/>");
  $sortingSelect.append("<option data-value='n'>name</option>");
  $sortingSelect.append("<option data-value='c'>color</option>");
  $sortingSelect.append("<option data-value='b'>brand</option>");
  $sortingSelect.append("<option data-value='s'>sold_out</option>");
  var index = window.location.hash.indexOf('sorting=');
  if (index !== -1) {
    var element = $sortingSelect.find("option[data-value=" + window.location.hash[index + 8] + "]");
    element.attr("selected", "selected");
    this.productsHandler.currentSortingBy = element.val();
  }
  return $sortingSelect;
}

{
  filter: {
    color: ['sd'],
    brand: [],
    available: 1
  },
  pagination: 9,
  sort: 'name'
}

#pagination=12;filter-color=ksn,sd,sd;filter-brancd=dawekf,asfne,asd,sort=name

FiltersHandler.prototype.getPaginationSelectStructure = function() {
  var $paginationSelect = $("<select data-type='pagination'/>");
  $paginationSelect.append("<option value='3'>3</option>");
  $paginationSelect.append("<option value='6'>6</option>");
  $paginationSelect.append("<option value='9'>9</option>");

  var index = window.location.hash.indexOf('pagination=');
  if (index !== -1) {
    $paginationSelect.find("option[value=" + window.location.hash[index + 11] + "]").attr("selected", "selected");
    this.productsHandler.productsPerPage = parseInt(window.location.hash[index + 11]);
  }

  return $paginationSelect;
}

FiltersHandler.prototype.getFilteredProducts = function() {
  var filters = this.filters;
  var filteredProducts = this.products.slice(0);
  var that = this;
  Object.keys(filters).forEach(function(item, index, array) {
    var filterType = item;
    var checkedElements = that.$filtersContainer.find("input[data-type=" + item + "]:checked");
    if (checkedElements.length === 0) {
      return;
    }
    if (filters[item]["type"] == "Boolean") {
      filteredProducts = filteredProducts.filter(function(item, index, array) {
        return item[filterType] == false;
      });
    } else {
      filteredProducts = filteredProducts.filter(function(item, index, array) {
        for (var i = 0; i < checkedElements.length; i++) {
          if (item[filterType] == checkedElements[i].value) {
            return true;
          }
        }
        return false;
      });
    }
  });
  return filteredProducts;
};

FiltersHandler.prototype.bindEvents = function() {
  var that = this;
  this.$filtersContainer.find("input[type=checkbox]").click(function() {
    that.updateHash(this.value);
    that.productsHandler.updateProducts(that.getFilteredProducts());
  });

  this.$paginationSelect.on("change", function() {
    that.updatePaginationHash(this.value);
    that.productsHandler.updateProductsPerPage(parseInt(this.value));
  });


  this.$sortingSelect.on("change", function() {
    that.updateSortingHash(this.value);
    that.productsHandler.sortProductsBy(this.value);
  });
};

FiltersHandler.prototype.updatePaginationHash = function(value) {
  window.location.hash = window.location.hash.replace(/,pagination=\d+/, "");
  window.location.hash += (",pagination=" + value);

}

FiltersHandler.prototype.updateSortingHash = function(value) {
  window.location.hash = window.location.hash.replace(/,sorting=[a-z]+/, "");
  window.location.hash += (",sorting=" + value);
}

FiltersHandler.prototype.updateHash = function(value) {
  value = value.replace(" ", "%20");
  var index = window.location.hash.split(",").indexOf(value);
  if (index == -1) {
    window.location.hash += "," + value;
  } else {
    window.location.hash = window.location.hash.replace("," + value, "");
  }
}

//---------------------------------------------------------

function Controller() {}

Controller.prototype.loadContent = function(response) {
  var filtersHandler = new FiltersHandler(response);
  filtersHandler.init();
};

$(document).ready(function() {
  var controller = new Controller();
  var ajaxHandler = new AJAXHandler("product.json", controller.loadContent, function() {});
    ajaxHandler.init();
});